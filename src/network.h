/**
 * @file network.h
 * @author Javor Dimitrov
 * @date 28 sep 2020
 *
 * @brief Network utils - header file.
 */

#ifndef __network_h__
#define __network_h__

#include <netinet/in.h>

/**
 * Open TCP port and listen.
 *
 * @param ipaddr IP address to bind server socket to (or INADDR_ANY).
 * @param port Server port.
 *
 * @return Server socket or -1 on error.
 */
int tcp_server(in_addr_t ipaddr, int port);

/**
 * Accept client connection from server socket.
 *
 * @param ssock Server socket.
 * @param[out] caddr Client address if non-null.
 *
 * @return Client socket or -1 on error.
 */
int tcp_accept(int ssock, struct sockaddr_in *caddr);

/**
 * Receive a line from TCP socket.
 *
 * @param sock TCP socket.
 *
 * @return Dynamically allocated memory with received line.
 *
 * @todo Create.
 */
char *recvline(int sock);

/**
 * printf() to socket.
 *
 * @param sock Network socket.
 * @param fmt Format string (see printf()).
 * @param ... Format arguments.
 *
 * @todo Create.
 */
void net_printf(int sock, const char *fmt, ...);

#endif /* network.h */
