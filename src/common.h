/**
 * @file common.h
 * @author Javor Dimitrov
 * @date 28 sep 2020
 *
 * @brief Common functions and structures for C Toolset project - header file.
 */

#ifndef __common_h__
#define __common_h__

/**
 * Allocate memory or immediately exit on failure.
 */
inline void *_malloc(size_t size);

/**
 * Reallocate memory or immediately exit on failure.
 */
inline void *_realloc(void *ptr, size_t size);

/**
 * Print out an error message with prefix and quit to OS.
 */
void err_sys(const char *msg);

/**
 * Print out an error message with prefix and quit to OS with status.
 */
void err_sys_s(const char *msg, int status);

#endif /* common.h */
