/**
 * @file cmd.h
 * @author Javor Dimitrov
 * @date 28 sep 2020
 *
 * @brief Command parser - header file.
 */

#ifndef __cmd_h__
#define __cmd_h__

/**
 * Parsed command structure.
 */
typedef struct {
  /*! Command */
  char *cmd;

  /*! Number of arguments */
  int nargs;

  /*! Arguments array */
  char **args;
} command_t;

/**
 * Parse command from string.
 *
 * @param line String.
 *
 * @return Pointer to command_t with parsed command.
 *
 * @todo Create.
 */
command_t *cmd_parse(const char *line);

/**
 * Free command_t structure.
 *
 * @todo Create.
 */
void cmd_free(command_t *cmd);

#endif /* cmd.h */
