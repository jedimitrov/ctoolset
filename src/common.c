/**
 * @file common.c
 * @author Javor Dimitrov
 * @date 28 sep 2020
 *
 * @brief Common functions and structures for C Toolset project.
 */

#include <stdio.h>
#include <stdlib.h>

#include "common.h"


inline void *_malloc(size_t size) {
    void *ptr = malloc(size);
    if(ptr == NULL)
        err_sys("_malloc() failed");
    return ptr;
}

inline void *_realloc(void *ptr, size_t size) {
    void *ptr2 = realloc(ptr, size);
    if(ptr == NULL)
        err_sys("_realloc() failed");
    return ptr2;
}

void err_sys(const char *msg)
{
    perror(msg);
    exit(-1);
}

void err_sys_s(const char *msg, int status)
{
    perror(msg);
    exit(status);
}