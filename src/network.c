/**
 * @file network.c
 * @author Javor Dimitrov
 * @date 28 sep 2020
 *
 * @brief Network utils.
 */

#include <stddef.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "network.h"


int tcp_server(in_addr_t ipaddr, int port)
{
    struct sockaddr_in saddr;
    int sock, enable = 1;

    /* Create a socket */
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1)
        return -1;

    /* Prepare address structure for bind */
    saddr.sin_addr.s_addr = ipaddr;
    saddr.sin_port = htons(port);
    saddr.sin_family = AF_INET;

    /* Reuse port if it's beed released recently */
    if(setsockopt(sock, SOL_SOCKET, SO_REUSEPORT | SO_REUSEADDR, &enable, sizeof(enable)) == -1)
        return -1;

    /* Bind socket to address */
    if(bind(sock, (struct sockaddr*)&saddr, sizeof(saddr)) == -1)
        return -1;

    /* Start listening */
    if(listen(sock, SOMAXCONN) == -1)
        return -1;

    return sock;
}

int tcp_accept(int ssock, struct sockaddr_in *caddr)
{
    struct sockaddr_in taddr;
    socklen_t clen;
    int csock;

    /* Accept client connection */
    clen = sizeof(caddr);
    csock = accept(ssock, (struct sockaddr*)&taddr, &clen);
    if(csock == -1)
        return -1;
    
    /* Copy client address */
    if(caddr != NULL)
        memcpy(caddr, &taddr, sizeof(struct sockaddr_in));

    return csock;
}
