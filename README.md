# C Toolset

### Prerequisites
1. Linux OS
1. ```gcc```
1. ```ar```
1. ```make```
1. Optionally ```doxygen```

### Build Instructions
1. ```cd c-toolset/src```
1. ```make```
1. Optionally ```make doc``` (requires doxygen)

### Usage Example
```gcc -o myproject myproject.c -L../c-toolset/lib -lcts```
